package ru.nsu.fit.services.rest;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Юля on 17.12.2016.
 */
public class RestService {
    public static WebTarget buildTarget(String login, String password) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(HttpAuthenticationFeature.basic(login, password));
        clientConfig.register(JacksonFeature.class);
        return ClientBuilder.newClient(clientConfig).target("http://localhost:8080/endpoint/rest");
    }

    public static Response createCustomer(WebTarget webTarget, Customer.CustomerData customerData) throws IOException {
        Invocation.Builder invocationBuilder =	webTarget.path("create_customer").request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity(JsonMapper.toJson(customerData, true), MediaType.APPLICATION_JSON));
    }

    public static Response getCustomers(WebTarget webTarget) {
        return webTarget.path("get_customers").request().get();
    }

    public static Response createPlan(WebTarget webTarget, Plan.PlanData planData) throws IOException {
        Invocation.Builder invocationBuilder =	webTarget.path("create_plan").request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity(JsonMapper.toJson(planData, true), MediaType.APPLICATION_JSON));
    }

    public static Response deletePlan(WebTarget webTarget, UUID planId) {
        Invocation.Builder invocationBuilder =	webTarget.path("delete_plan/" + planId).request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity("", MediaType.APPLICATION_JSON));
    }

    public static Response getUserId(WebTarget webTarget) {
        return webTarget.path("get_user_id/").request().get();
    }

    public static Response getCustomerId(WebTarget webTarget, String login) {
        return webTarget.path("get_customer_id/" + login).request().get();
    }

    public static Response getCustomerData(WebTarget webTarget) {
        return webTarget.path("get_customer_data").request().get();
    }

    public static Response getUsers(WebTarget webTarget) {
        return webTarget.path("get_users").request().get();
    }

    public static Response replenishBalance(WebTarget webTarget, int moneyAmount) {
        Invocation.Builder invocationBuilder =	webTarget.path("replenish_balance/" + moneyAmount).request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity("", MediaType.APPLICATION_JSON));
    }

    public static Response getPlans(WebTarget webTarget) {
        return webTarget.path("get_plans").request().get();
    }

    public static Response buyPlan(WebTarget webTarget, UUID planId) {
        Invocation.Builder invocationBuilder =	webTarget.path("buy_plan/" + planId).request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity("", MediaType.APPLICATION_JSON));
    }

    public static Response getCustomerSubscriptions(WebTarget webTarget) {
        return webTarget.path("get_subscriptions").request().get();
    }

    public static Response getUserSubscriptions(WebTarget webTarget) {
        return webTarget.path("get_user_subscriptions").request().get();
    }

    public static Response assignSubscription(WebTarget webTarget, UUID userId, UUID subscriptionId) {
        Invocation.Builder invocationBuilder =	webTarget.path("assign_subscription/" + userId + "/" + subscriptionId).request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity("", MediaType.APPLICATION_JSON));
    }

    public static Response removeSubscription(WebTarget webTarget, UUID userId, UUID subscriptionId) {
        Invocation.Builder invocationBuilder =	webTarget.path("remove_subscription/" + userId + "/" + subscriptionId).request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity("", MediaType.APPLICATION_JSON));
    }

    public static Response createUser(WebTarget webTarget, User.UserData userData) throws IOException {
        Invocation.Builder invocationBuilder =	webTarget.path("create_user").request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity(JsonMapper.toJson(userData, true), MediaType.APPLICATION_JSON));
    }

    public static Response deleteUser(WebTarget webTarget, String login) {
        Invocation.Builder invocationBuilder =	webTarget.path("delete_user/" + login).request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity("", MediaType.APPLICATION_JSON));
    }

    public static Response getUserData(WebTarget webTarget) {
        return webTarget.path("get_user_data").request().get();
    }

    public static Response changeRole(WebTarget webTarget, UUID userId, User.UserData.UserRole newUserRole) {
        Invocation.Builder invocationBuilder =	webTarget.path("change_role/" + userId + "/" + newUserRole).request(MediaType.APPLICATION_JSON);
        return invocationBuilder.post(Entity.entity("", MediaType.APPLICATION_JSON));
    }
}
