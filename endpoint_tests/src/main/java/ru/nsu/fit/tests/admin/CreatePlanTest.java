package ru.nsu.fit.tests.admin;

import com.fasterxml.jackson.core.type.TypeReference;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Юля on 07.12.2016.
 */
@Title("CreatePlanTest")
public class CreatePlanTest {
    private void createPlan(Plan.PlanData creatingPlanData) {
        WebTarget webTarget = Generator.createAdminWebTarget();
        try {
            Response response = RestService.createPlan(webTarget, creatingPlanData);
            Logger.checkResponse(response, "The plan with name \"" + creatingPlanData.getName() + "\" was created", 200);

            response = RestService.getPlans(webTarget);
            String textResponse = response.readEntity(String.class);
            List<Plan> plans = JsonMapper.fromJson(textResponse, new TypeReference<ArrayList<Plan>>(){});
            boolean ok = false;
            for (Plan plan: plans) {
                if (plan.getData().getName().equals(creatingPlanData.getName())){

                    if (    plan.getData().getDetails().equals(creatingPlanData.getDetails()) &&
                            plan.getData().getFeePerUnit() == creatingPlanData.getFeePerUnit() &&
                            plan.getData().getMaxSeats() == creatingPlanData.getMaxSeats() &&
                            plan.getData().getPlanType() == creatingPlanData.getPlanType())
                        ok = true;

                    break;
                }
            }

            Logger.checkBool(ok, "The new plan was created by Admin");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Title("Check internal plan creation")
    @Description("Create internal Plan via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("PlanCreation")
    public void createInternalPlan() {
        createPlan(Generator.createInternalPlanData());
    }

    @Test
    @Title("Check external plan creation")
    @Description("Create external Plan via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("PlanCreation")
    public void createExternalPlan() {
        createPlan(Generator.createExternalPlanData());
    }

}
