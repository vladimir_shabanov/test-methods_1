package ru.nsu.fit.tests.customer;

import com.fasterxml.jackson.core.type.TypeReference;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Юля on 17.12.2016.
 */
public class DeleteUserTest {

    @Test
    @Title("Delete User")
    @Description("Delete User via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("DeleteUser")
    public void deleteUser() {
        WebTarget webTarget = Generator.createCustomerWebTarget();
        try {
            Response response = RestService.getUsers(webTarget);
            List<User> users = JsonMapper.fromJson(response.readEntity(String.class), new TypeReference<ArrayList<User>>(){});
            User deletingUser = users.get(0);

            String login =  deletingUser.getData().getLogin();
            response = RestService.deleteUser(webTarget, login);
            Logger.checkResponse(response, "The plan with id \"" + login + "\" was deleted", 200);

            response = RestService.getUsers(webTarget);
            users = JsonMapper.fromJson(response.readEntity(String.class), new TypeReference<ArrayList<User>>(){});
            boolean ok = true;
            for (User user: users) {
                if (user.getData().getLogin().equals(login)){
                    ok = false;
                    break;
                }
            }

            Logger.checkBool(ok, "The user was deleted");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
