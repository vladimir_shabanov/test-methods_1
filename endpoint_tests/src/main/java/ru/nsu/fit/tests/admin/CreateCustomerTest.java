package ru.nsu.fit.tests.admin;

import com.fasterxml.jackson.core.type.TypeReference;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Customer.CustomerData;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.nsu.fit.utils.Generator.createCustomerData;

/**
 * Created by Юля on 18.12.2016.
 */
@Title("CreateCustomer")
public class CreateCustomerTest {
    @Test
    @Title("Check customer creation")
    @Description("Create customer via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("CustomerCreation")
    public void createCustomer() {
        WebTarget webTarget = Generator.createAdminWebTarget();
        try {
            CustomerData customerData = createCustomerData();
            Response response = RestService.createCustomer(webTarget, customerData);
            Logger.checkStatus(response, 200);

            String textResponse = response.readEntity(String.class);
            CustomerData createdCustomerData = JsonMapper.fromJson(textResponse, CustomerData.class);
            boolean createdCustomerCorrect = createdCustomerData.getFirstName().equals(customerData.getFirstName()) &&
                    createdCustomerData.getLastName().equals(customerData.getLastName()) &&
                    createdCustomerData.getPass().equals(customerData.getPass()) &&
                    createdCustomerData.getLogin().equals(customerData.getLogin()) &&
                    createdCustomerData.getMoney() == customerData.getMoney();
            Logger.checkBool(createdCustomerCorrect, "JSON for created Customer isn't correct");

            response = RestService.getCustomers(webTarget);
            textResponse = response.readEntity(String.class);
            System.out.println(textResponse);
            List<Customer> Customers = JsonMapper.fromJson(textResponse, new TypeReference<ArrayList<Customer>>() {
            });
            boolean ok = false;
            for (Customer Customer : Customers) {
                if (Customer.getData().getLogin().equals(customerData.getLogin())) {

                    if (Customer.getData().getFirstName().equals(customerData.getFirstName()) &&
                            Customer.getData().getLastName().equals(customerData.getLastName()) &&
                            Customer.getData().getPass().equals(customerData.getPass()) &&
                            Customer.getData().getMoney() == customerData.getMoney()) {
                        ok = true;
                    }

                    break;
                }
            }

            Logger.checkBool(ok, "The new Customer was created by Admin");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
