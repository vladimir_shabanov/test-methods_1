package ru.nsu.fit.tests.customer;

import com.fasterxml.jackson.core.type.TypeReference;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Map;
import java.util.SortedMap;

/**
 * Created by Юля on 18.12.2016.
 */
public class GetCustomerIdTest {
    @Test
    @Title("Get Customer ID")
    @Description("Get Customer ID via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Get Customer ID")
    public void GetCustomerID() {

        WebTarget webTarget = Generator.createCustomerWebTarget();
        Response response = RestService.getCustomerId(webTarget, Generator.getDefaultCustomer());

        Logger.checkStatus(response, 200);
        try {
            Map<String, String> responseMap = JsonMapper.fromJsonToMap(response.readEntity(String.class), new TypeReference<SortedMap<String,String>>(){});
            for (String key: responseMap.keySet()) {
                System.out.print("key :" + key);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
