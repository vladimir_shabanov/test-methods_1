package ru.nsu.fit.tests;

import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;

import javax.jws.soap.SOAPBinding;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import java.io.IOException;

import static ru.nsu.fit.utils.Generator.createCustomerData;
import static ru.nsu.fit.utils.Generator.defaultCustomer;
import static ru.nsu.fit.utils.Generator.defaultUser;

/**
 * Created by Юля on 18.12.2016.
 */
public class CreateDefaultUsersTest {

    @Test
    public void createDefaultCustomer() {
        WebTarget webTarget = Generator.createAdminWebTarget();
        try {
            Customer.CustomerData customerData = defaultCustomer;
            Response response = RestService.createCustomer(webTarget, customerData);
            Logger.checkStatus(response, 200);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
        @Test(dependsOnMethods = "createDefaultCustomer")
    public void createDefaultUser() {
            WebTarget webTarget = Generator.createCustomerWebTarget();
            try {
                User.UserData userData = defaultUser;
                Response response = RestService.createUser(webTarget, userData);
                Logger.checkStatus(response, 200);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

}
