package ru.nsu.fit.tests.customer;

import com.fasterxml.jackson.core.type.TypeReference;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Юля on 18.12.2016.
 */
public class BuyPlanTest {
    @Test
    @Title("Check buy plan")
    @Description("Buy plan via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("PurchasePlan")
    public void BuyPlan() {
        WebTarget webTarget = Generator.createCustomerWebTarget();

        Response response = RestService.getPlans(webTarget);
        String textResponse = response.readEntity(String.class);
        List<Plan> plans = null;
        try {
            plans = JsonMapper.fromJson(textResponse, new TypeReference<ArrayList<Plan>>() {});
            Plan selectedPlan = plans.get(0);
            response = RestService.buyPlan(webTarget, selectedPlan.getId());
            Logger.checkResponse(response, "The plan with id \"" + selectedPlan.getId() + "\" was bought", 200);

            // check subscription
            response = RestService.getCustomerSubscriptions(webTarget);
            textResponse = response.readEntity(String.class);
            System.out.println(textResponse);
            List<Subscription> subscriptions = JsonMapper.fromJson(textResponse, new TypeReference<ArrayList<Subscription>>() {
            });
            boolean ok = false;
            for (Subscription subscription : subscriptions) {
                if (subscription.getPlanId().equals(selectedPlan.getId())) {
                    ok = true;
                    break;
                }
            }
            Logger.checkBool(ok, "Plan was bought.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
