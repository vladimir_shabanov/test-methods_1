package ru.nsu.fit.tests.user;

import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.log4j.lf5.LF5Appender;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * Created by Юля on 18.12.2016.
 */
public class GetUserIdTest {
    @Test
    @Title("Get User ID")
    @Description("Get User ID via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Get User ID")
    public void GetUserId() {
        WebTarget webTarget = Generator.createUserWebTarget();
        Response response = RestService.getUserId(webTarget);

        Logger.checkStatus(response, 200);
        try {
            Map<String, String> responseMap = JsonMapper.fromJsonToMap(response.readEntity(String.class), new TypeReference<SortedMap<String,String>>(){});
            boolean ok = false;
            String id = null;
            for (String key: responseMap.keySet()) {
                if (key.equals("id")) {
                    id = responseMap.get(key);
                    ok = true;
                }
            }
            Logger.checkBool(ok, "User ID received");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
