package ru.nsu.fit.tests.admin;

import com.fasterxml.jackson.core.type.TypeReference;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Юля on 17.12.2016.
 */
public class DeletePlanTest {

    @Test
    @Title("Delete Plan")
    @Description("Delete Plan via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("DeletePlan")
    public void deletePlan() {
        WebTarget webTarget = Generator.createAdminWebTarget();
        try {
            Response response = RestService.getPlans(webTarget);
            List<Plan> plans = JsonMapper.fromJson(response.readEntity(String.class), new TypeReference<ArrayList<Plan>>(){});
            Plan deletingPlan = plans.get(0);
            for (Plan plan: plans) {
                if (plan.getData().getPlanType() == Plan.PlanType.INTERNAL) {
                    deletingPlan = plan;
                    break;
                }
            }

            response = RestService.deletePlan(webTarget, deletingPlan.getId());
            Logger.checkResponse(response, "The plan with id \"" + deletingPlan.getId() + "\" was deleted", 200);

            response = RestService.getPlans(webTarget);
            plans = JsonMapper.fromJson(response.readEntity(String.class), new TypeReference<ArrayList<Plan>>(){});
            boolean ok = true;
            for (Plan plan: plans) {
                if (plan.getData().getName().equals(deletingPlan.getData().getName())){
                    ok = false;
                    break;
                }
            }

            Logger.checkBool(ok, "The plan was deleted by Admin");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
