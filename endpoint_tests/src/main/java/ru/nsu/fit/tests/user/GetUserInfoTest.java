package ru.nsu.fit.tests.user;

import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * Created by Юля on 18.12.2016.
 */
@Title("GetUserInfoTest")
public class GetUserInfoTest {
    @Test
    @Title("Get User Info")
    @Description("Get User Info via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Get User Info")
    public void getUserInfo() {
        WebTarget webTarget = Generator.createUserWebTarget();
        Response response = RestService.getUserData(webTarget);

        Logger.checkStatus(response, 200);

        User user = JsonMapper.fromJson(response.readEntity(String.class), User.class);

    }
}
