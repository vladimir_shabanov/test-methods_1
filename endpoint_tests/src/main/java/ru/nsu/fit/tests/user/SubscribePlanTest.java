package ru.nsu.fit.tests.user;

import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * Created by Юля on 18.12.2016.
 */
public class SubscribePlanTest {
    public void subscribePlan() {
        WebTarget webTarget = Generator.createUserWebTarget();
        // get money
        Response response = RestService.getCustomerData(webTarget);
        String textResponse = response.readEntity(String.class);
        int customerBalance = JsonMapper.fromJson(textResponse, Customer.CustomerData.class).getMoney();
    }
}
