package ru.nsu.fit.tests.customer;

import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.utils.Generator;
import ru.nsu.fit.utils.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by Юля on 18.12.2016.
 */
@Title("GetCustomerInfoTest")
public class GetCustomerInfoTest {
    private Customer.CustomerData createCustomer() {
        WebTarget webTarget = Generator.createAdminWebTarget();
        Customer.CustomerData customerData = Generator.createCustomerData();
        try {
            RestService.createCustomer(webTarget, customerData);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return customerData;
    }

    @Test
    @Title("Get Customer data")
    @Description("Get Customer data with ID via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Get Customer data")
    public void getUserInfo() {
        Customer.CustomerData customerData = createCustomer();

        WebTarget webTarget = Generator.createCustomerWebTarget();
        Response response = RestService.getCustomerData(webTarget);
        Logger.checkStatus(response, 200);

        Customer customer = JsonMapper.fromJson(response.readEntity(String.class), Customer.class);

    }

}
