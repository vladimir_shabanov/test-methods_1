package ru.nsu.fit.utils;

import org.testng.Assert;
import ru.nsu.fit.shared.AllureUtils;

import javax.ws.rs.core.Response;

/**
 * Created by Юля on 17.12.2016.
 */
public class Logger {
    private static void writeToAllure(int status, String textResponse) {
        AllureUtils.saveTextLog("Response with status "+ status + ": " + textResponse);
    }

    private static void writeToAllure(String message) {
        AllureUtils.saveTextLog(message);
    }

    public static void checkResponse(Response response, String expectedText, int expectedStatus) {
        String textResponse = response.readEntity(String.class);
        int status = response.getStatus();

        Assert.assertEquals(status, expectedStatus);
        Assert.assertEquals(textResponse, expectedText);
        writeToAllure(status, textResponse);
    }

    public static void checkStatus(Response response, int expectedStatus) {
        int status = response.getStatus();

        Assert.assertEquals(status, expectedStatus);
        writeToAllure("Status is : " + status);
    }

    public static void checkBool(boolean condition, String message) {
        Assert.assertTrue(condition);
        writeToAllure(message);
    }

    public static void checkObjects(Object actual, Object expected, String message) {
        Assert.assertEquals(actual, expected);
        writeToAllure(message);
    }
}
