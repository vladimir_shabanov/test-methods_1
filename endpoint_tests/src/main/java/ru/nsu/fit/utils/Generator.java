package ru.nsu.fit.utils;

import io.codearte.jfairy.Fairy;
import ru.nsu.fit.services.rest.RestService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.services.rest.RestService;

import javax.ws.rs.client.WebTarget;

/**
 * Created by Юля on 17.12.2016.
 */
public class Generator {
    private static String admin_login = "admin";
    private static String admin_pass = "setup";
    private static Fairy fairy = Fairy.create();
    public static Customer.CustomerData defaultCustomer =
            new Customer.CustomerData("Bob", "Marley", "Bob@mail.ru", "qwe123", 100000);
    public static User.UserData defaultUser =
            new User.UserData("Brain", "Molko", "placebo@mail.com", "123qwe",
                    User.UserData.UserRole.BILLING_ADMINISTRATOR);

    public static WebTarget createAdminWebTarget() {
        return RestService.buildTarget(admin_login, admin_pass);
    }

    public static WebTarget createCustomerWebTarget() {
        return RestService.buildTarget(defaultCustomer.getLogin(), defaultCustomer.getPass());
    }

    public static WebTarget createUserWebTarget() {
        return RestService.buildTarget(defaultUser.getLogin(), defaultUser.getPass());
    }

    public static String getDefaultCustomer() {
        return defaultCustomer.getLogin();
    }

    public static Customer.CustomerData createCustomerData() {
        return new Customer.CustomerData(
                fairy.person().firstName(),
                fairy.person().lastName(),
                fairy.person().email(),
                "123789456",
                100);
    }

    public static User.UserData createUserData() {
        return new User.UserData(
                fairy.person().firstName(),
                fairy.person().lastName(),
                fairy.person().email(),
                "123789456",
                User.UserData.UserRole.USER);
    }

    public static Plan.PlanData createExternalPlanData() {
        return new Plan.PlanData(
                fairy.person().firstName(),
                fairy.person().lastName(),
                100,
                100,
                Plan.PlanType.EXTERNAL);
    }

    public static Plan.PlanData createInternalPlanData() {
        return new Plan.PlanData(
                fairy.person().firstName(),
                fairy.person().lastName(),
                100,
                100,
                Plan.PlanType.EXTERNAL);
    }
}