package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.glassfish.jersey.internal.util.Base64;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Path("/rest")
public class RestService {
    //Слайд с требованиями к Customer
    @RolesAllowed("ADMIN")
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        System.out.println(customerDataJson);
        try {
            Customer.CustomerData customerData = JsonMapper.fromJson(customerDataJson, Customer.CustomerData.class);
            DBService.createCustomer(customerData);
            return Response.status(200).entity(JsonMapper.toJson(customerData, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (IOException e) {
            return Response.status(400).entity(e.fillInStackTrace()).build();
        }
    }

    @RolesAllowed("ADMIN")
    @POST
    @Path("/create_plan")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPlan(String planDataJson) {
        try {
            System.out.println("Try to create plan");
            Plan.PlanData plan = JsonMapper.fromJson(planDataJson, Plan.PlanData.class);
            DBService.createPlan(plan);
            return Response.status(200).entity("The plan with name \"" + plan.getName() + "\" was created").build();
        } catch (IllegalArgumentException ex) {
            System.out.println("Lol");
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("ADMIN")
    @POST
    @Path("/delete_plan/{planId}")
    public Response deletePlan(@PathParam("planId") UUID planId) {
        try {
            DBService.deletePlan(planId);
            return Response.status(200).entity("The plan with id \"" + planId + "\" was deleted").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({"ADMIN", "CUSTOMER"})
    @GET
    @Path("/get_customer_id/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.status(200).entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/get_customer_data/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerData(@HeaderParam("Authorization") String authorization) {
        try {
            UUID customerId = getIdByAuth(authorization);
            Customer.CustomerData customerData = DBService.getCustomerDataById(customerId);
            String response = JsonMapper.toJson(customerData, true);
            return Response.status(200).entity(response).build();
        } catch (Exception ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/get_users/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@HeaderParam("Authorization") String authorization) {
        try {
            UUID customerId = getIdByAuth(authorization);
            List<User> users = DBService.getUsersByCustomerId(customerId);
            String response = JsonMapper.toJson(users, true);
            return Response.status(200).entity(response).build();
        } catch (Exception ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("ADMIN")
    @GET
    @Path("/get_customers/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomers(@HeaderParam("Authorization") String authorization) {
        try {
            //UUID customerId = getIdByAuth(authorization);
            List<Customer> customers = DBService.getCustomers();
            String response = JsonMapper.toJson(customers, true);
            return Response.status(200).entity(response).build();
        } catch (Exception ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed({"ADMIN", "CUSTOMER"})
    @GET
    @Path("/get_plans/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlans() {
        try {
            List<Plan> plans = DBService.getPlans();
            String response = JsonMapper.toJson(plans, true);
            return Response.status(200).entity(response).build();
        } catch (Exception ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/get_subscriptions/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptions(@HeaderParam("Authorization") String authorization) {
        try {
            UUID customerId = getIdByAuth(authorization);
            List<Subscription> subscriptions = DBService.getSubscriptionsByCustomerId(customerId);
            String response = JsonMapper.toJson(subscriptions, true);
            return Response.status(200).entity(response).build();
        } catch (Exception ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/buy_plan/{planId}")
    public Response buyPlan(@HeaderParam("Authorization") String auth, @PathParam("planId") UUID planId) {
        System.out.println("buyPlan");
        try {
            UUID customerId = getIdByAuth(auth);
            DBService.buyPlan(customerId, planId);
            return Response.status(200).entity("The plan with id \"" + planId + "\" was bought").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/up_balance/{amount}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response upBalance(@HeaderParam("Authorization") String auth, @PathParam("amount") Integer amount) {
        try {
            UUID customerId = getIdByAuth(auth);
            if (amount <= 0)
                return Response.status(400).entity("Amount value must be positive").build();

            DBService.updateBalance(customerId, amount);

            return Response.status(200).entity("Balance were updated on " + amount).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    //Слайд с требованиями к Пользователю

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/create_user")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(@HeaderParam("Authorization") String auth, String userDataJson) {
        try {
            UUID customerId = getIdByAuth(auth);
            User.UserData userData = JsonMapper.fromJson(userDataJson, User.UserData.class);
            DBService.createUser(userData, customerId);
            return Response.status(200).entity("The user " + userData.getLogin() + " has been created").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/delete_user/{login}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("login") String login) {
        try {
            DBService.deleteUser(login);
            return Response.status(200).entity("The user with login \"" + login + "\" was deleted").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/subscribe_on_user/{user_id}/{subscription_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response subscribeOnUser(@HeaderParam("Authorization") String auth, @PathParam("user_id") UUID userId, @PathParam("subscription_id") UUID subscriptionId) {
        try {
            UUID customerId = getIdByAuth(auth);
            DBService.subscribeSubscriptionOnUser(customerId, userId, subscriptionId);
            return Response.status(200).entity("Subscription with id " + subscriptionId + " was subscribed on User with id " + userId).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/unsubscribe_from_user/{user_id}/{subscription_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response unsubscribeFromUser(@HeaderParam("Authorization") String auth, @PathParam("user_id") UUID userId, @PathParam("subscription_id") UUID subscriptionId) {
        try {
            UUID customerId = getIdByAuth(auth);
            DBService.unsubscribeSubscriptionFromUser(customerId, userId, subscriptionId);
            return Response.status(200).entity("Subscription with id " + subscriptionId + " was unsubscribed from User with id " + userId).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/change_role/{user_id}/{role}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeRole(@HeaderParam("Authorization") String auth, @PathParam("user_id") UUID userId, @PathParam("role") User.UserData.UserRole role) {
        try {
            UUID customerId = getIdByAuth(auth);
            DBService.changeRole(customerId, userId, role);
            return Response.status(200).entity("UserRole of user with id " + userId + " was changed on " + role).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("USER")
    @GET
    @Path("/get_user_id/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserId(@HeaderParam("Authorization") String auth) {
        try {
            UUID id = getIdByAuth(auth);
            return Response.status(200).entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    @RolesAllowed({"USER","CUSTOMER"})
    @GET
    @Path("/get_user_data")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserData(@HeaderParam("Authorization") String auth) {
        try {
            UUID userId = getIdByAuth(auth);
            User.UserData userData = DBService.getUserDataById(userId);
            String response = JsonMapper.toJson(userData, true);

            return Response.status(200).entity(response).build();

        } catch (Exception ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
    private UUID getIdByAuth(String authentication) {
        String str = authentication.split(" ")[1];
        str = Base64.decodeAsString(str);
        String login =  str.split(":")[0];

        return DBService.getIdByLogin(login);
    }
}