package ru.nsu.fit.endpoint.service.database.Exceptions;

/**
 * Created by Vladimir on 24.10.2016.
 */
public class CustomerException extends IllegalArgumentException {
    public CustomerException(String message) {
        super(message);
    }
    public CustomerException() {

    }
}
