package ru.nsu.fit.endpoint.service.database.Exceptions;

/**
 * Created by Vladimir on 24.10.2016.
 */
public class UserException extends IllegalArgumentException {
    public UserException() {

    }

    public UserException(String message) {
        super(message);
    }
}
