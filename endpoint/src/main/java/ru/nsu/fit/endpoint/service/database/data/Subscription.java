package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription {
    private UUID id;
    private UUID customerId;
    private UUID planId;
    private int usedSeats;

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public UUID getId() {
        return id;
    }

    public enum SubscriptionType {
        PROVISIONING("Provisioning"),
        DONE("Done");

        private String typeName;

        SubscriptionType(String typeName) {
            this.typeName = typeName;
        }

        public String getTypeName() {
            return typeName;
        }

    }
    private SubscriptionType subscriptionType;

    @JsonCreator
    public Subscription (@JsonProperty("id")UUID id,
                         @JsonProperty("customerId")UUID customerId,
                         @JsonProperty("planId")UUID planId,
                         @JsonProperty("usedSeats")int usedSeats,
                         @JsonProperty("subscriptionType")SubscriptionType subscriptionType) {
        this.id = id;
        this.customerId = customerId;
        this.planId = planId;
        this.usedSeats = usedSeats;
        this.subscriptionType = subscriptionType;
    }

    public int getUsedSeats() {
        return usedSeats;
    }

    public UUID getPlanId() {
        return planId;
    }
}