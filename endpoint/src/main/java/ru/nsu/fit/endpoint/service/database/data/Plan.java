package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.nsu.fit.endpoint.service.database.Exceptions.PlanException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Plan extends Entity<Plan.PlanData> {
    @JsonProperty("id")
    private UUID id;

    public UUID getId() {
        return id;
    }

    @JsonCreator
    public Plan(@JsonProperty("id") UUID id,
                @JsonProperty("planData") PlanData planData) throws PlanException{
        super(planData);
        this.id = id;
    }

    public enum PlanType {
        EXTERNAL("External"),
        INTERNAL("Internal");

        private String typeName;

        private PlanType(String typeName) {
            this.typeName = typeName;
        }

        public String getTypeName() {
            return typeName;
        }

    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PlanData {
        /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
        @JsonProperty("name")
        private String name;
        /* Длина не больше 1024 символов и не меньше 1 включительно */
        @JsonProperty("details")
        private String details;

        public String getName() {
            return name;
        }

        public String getDetails() {
            return details;
        }

        public int getMaxSeats() {
            return maxSeats;
        }

        public int getFeePerUnit() {
            return feePerUnit;
        }

        /* Не больше 999999 и не меньше 1 включительно */
        @JsonProperty("maxSeats")
        private int maxSeats;
        /* Больше ли равно 0 но меньше либо равно 999999 */
        @JsonProperty("feePerUnit")
        private int feePerUnit;
        @JsonProperty("planType")
        private PlanType planType;

        private PlanData() {

        }

        public PlanType getPlanType() {
            return planType;
        }

        @JsonCreator
        public PlanData(@JsonProperty("name") String name,
                        @JsonProperty("details") String details,
                        @JsonProperty("maxSeats") int maxSeats,
                        @JsonProperty("feePerUnit") int feePerUnit,
                        @JsonProperty("planType") PlanType planType) throws PlanException {
            this.name = name;
            this.details = details;
            this.maxSeats = maxSeats;
            this.feePerUnit = feePerUnit;
            this.planType = planType;

            validateName(name);
            validateDetails(details);
            validateMaxSeats(maxSeats);
            validateFeePerUnit(feePerUnit);
        }

        public void validateName(String name) throws PlanException {
            if (name == null) {
                throw new PlanException("Name is null");
            }
            if (name.length() < 2) {
                throw new PlanException("Name is too short");
            }
            if (name.length() > 128) {
                throw new PlanException("Name is too long");
            }
            for (int i = 0; i < name.length(); i++) {
                if (!Character.isAlphabetic(name.charAt(i)))
                    throw new PlanException("Name contains special symbols");
            }
        }

        public void validateDetails(String details) throws PlanException {
            if (details == null) {
                throw new PlanException("Details are null");
            }
            if (details.length() < 1) {
                throw new PlanException("Details are too short");
            }
            if (details.length() > 1024) {
                throw new PlanException("Details are too long");
            }
        }

        public void validateMaxSeats(int maxSeats) throws PlanException {
            if (maxSeats < 1) {
                throw new PlanException("MaxSeats value too small");
            }
            if (maxSeats > 999999) {
                throw new PlanException("MaxSeats value too big");
            }
        }

        public void validateMinSeats(int minSeats, int maxSeats) throws PlanException {

            if (minSeats < 1) {
                throw new PlanException("MinSeats value too small");
            }
            if (minSeats > 999999) {
                throw new PlanException("MinSeats value too big");
            }
            if (minSeats > maxSeats) {
                throw new PlanException("MinSeats bigger than MaxSeats");
            }
        }

        public void validateFeePerUnit(int feePerUnit) throws PlanException{
            if (feePerUnit < 0) {
                throw new PlanException("feePerUnit too small");
            }
            if (feePerUnit > 999999) {
                throw new PlanException("feePerUnit too big");
            }
        }
    }


}
