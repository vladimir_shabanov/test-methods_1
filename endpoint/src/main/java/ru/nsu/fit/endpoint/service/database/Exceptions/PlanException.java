package ru.nsu.fit.endpoint.service.database.Exceptions;


/**
 * Created by Vladimir on 24.10.2016.
 */
public class PlanException extends IllegalArgumentException {
    public PlanException() {

    }

    public PlanException(String message) {
        super(message);
    }
}
