package ru.nsu.fit.endpoint.service.database;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.service.database.Exceptions.CustomerException;
import ru.nsu.fit.endpoint.service.database.Exceptions.PlanException;
import ru.nsu.fit.endpoint.service.database.Exceptions.UserException;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, money) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_USERS_BY_CUSTOMER = "SELECT first_name, last_name, login, pass, user_role, id FROM USER_TEST WHERE customer_id='%s'";
    private static final String SELECT_SUBSCRIPTION_BY_USER_ID =    "SELECT sub.id, sub.customer_id, sub.plan_id, sub.subscription_type, sub.used_seats " +
            "FROM SUBSCRIPTION sub, TEST_USER user, USER_ASSIGNMENT u_a " +
            "WHERE (u_a.user_id='%s') AND" +
            "(u_a.subscription_id = sub.id)";
    private static final String SELECT_SUBSCRIPTION_BY_CUSTOMER_ID = "SELECT * FROM SUBSCRIPTION WHERE customer_id='%s'";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, max_seats, fee_per_seat, plan_type) values ('%s', '%s', '%s', '%s', '%s', '%s')";
    private static final String INSERT_USER = "INSERT INTO USER(id, customer_id, first_name, last_name, login, pass, user_role) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
    private static final String DELETE_PLAN = "DELETE FROM PLAN WHERE id='%s'";

    private static final String SELECT_PLAN_BY_ID = "SELECT * FROM PLAN where id='%s'";
    private static final String SELECT_USER = "SELECT id FROM USER WHERE login='%s'";
    private static final String SELECT_CUSTOMER_DATA = "SELECT first_name, last_name, login, pass, money FROM CUSTOMER WHERE id='%s'";
    private static final String SELECT_PLAN_DATA = "SELECT name, details, maxSeats, feePerUnit, plan_type from PLAN WHERE id='%s'";
    private static final String SELECT_PLANS = "SELECT * FROM PLAN";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";
    private static final String SELECT_SUBSCRIPTION_BY_ID = "SELECT * FROM SUBSCRIPTION where id='%s'";
    private static final String SELECT_CUSTOMER_BY_ID = "SELECT * FROM CUSTOMER WHERE id='%s'";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(customer_id, id, plan_id, used_seats, subscription_type) values ('%s', '%s', '%s', '%s','%s')";
    private static final String DELETE_USER = "DELETE FROM USER WHERE login='%s'";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM USER WHERE id='%s'";
    private static final String SELECT_USER_COUNT_SUBSCRIPTIONS = "SELECT COUNT(subscription_id) FROM USER_ASSIGNMENT WHERE user_id='%s'";
    private static final String SELECT_USER_SUBSCRIPTIONS_BY_ID = "SELECT subscription_id FROM USER_ASSIGNMENT WHERE user_id='%s'";
    private static final String INSERT_USER_ASSIGNMENT = "INSERT INTO USER_ASSIGNMENT(user_id, subscription_id) values ('%s', '%s')";
    private static final String SELECT_ID_CUSTOMER_FROM_USER = "SELECT customer_id FROM USER WHERE id='%s'";
    private static final String SELECT_USER_PASSWORD_BY_LOGIN = "SELECT pass FROM USER WHERE login='%s'";
    private static final String SELECT_CUSTOMER_PASSWORD_BY_LOGIN = "SELECT pass FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_USER_ASSIGNMENT_SUBSCRIPTION = "SELECT * FROM USER_ASSIGNMENT WHERE user_id='%s' AND subscription_id='%s'";
    private static final String DELETE_USER_ASSIGNMENT = "DELETE FROM USER_ASSIGNMENT WHERE user_id='%s' AND subscription_id='%s'";
    private static final String UPDATE_SUBSCRIPTION = "UPDATE SUBSCRIPTION subscription_type='%s' WHERE id='%s'";
    private static final String SELECT_PLAN_TYPE = "SELECT plan_type FROM PLAN WHERE id='%s'";
    private static final String SELECT_PLAN_ID = "SELECT plan_id FROM SUBSCRIPTION WHERE customer_id='%s'";

    private static final Logger logger = LoggerFactory.getLogger("DB_LOG");
    private static final Object generalMutex = new Object();
    private static Connection connection;

    static {
        init();
    }

    public static void createCustomer(Customer.CustomerData customerData) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            Customer customer = new Customer(customerData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customer.getId(),
                                customer.getData().getFirstName(),
                                customer.getData().getLastName(),
                                customer.getData().getLogin(),
                                customer.getData().getPass(),
                                customer.getData().getMoney()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void createPlan(Plan.PlanData planData) {
        synchronized (generalMutex) {
            logger.info("Try to create plan");

            try {
                Statement statement = connection.createStatement();

                statement.executeUpdate(
                        String.format(
                                INSERT_PLAN,
                                UUID.randomUUID(),
                                planData.getName(),
                                planData.getDetails(),
                                planData.getMaxSeats(),
                                planData.getFeePerUnit(),
                                planData.getPlanType()));
            } catch (SQLException e) {
                System.out.println("SqlException");
                logger.debug(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public static void deletePlan(UUID planId) {
        synchronized (generalMutex) {
            logger.info("Try to delete plan");
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_DATA,
                                planId));
                if (!rs.next())
                    throw new IllegalArgumentException("The plan with id \"" + planId + "\" doesn't exist");
                else {
                    statement = connection.createStatement();
                    statement.executeQuery(String.format(DELETE_PLAN, planId));
                }
            } catch (SQLException e) {
                logger.debug(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    public static UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get customer id by login");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public static void createUser(User.UserData userData, UUID customerId) {
        synchronized (generalMutex) {
            logger.info("Try to create user with customer id");
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_USER,
                                UUID.randomUUID(),
                                customerId,
                                userData.getFirstName(),
                                userData.getLastName(),
                                userData.getLogin(),
                                userData.getPass(),
                                userData.getUserRole().getRoleName()));
            } catch (SQLException e) {
                logger.debug(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }
    public static List<User> getUsersByCustomerId(UUID customerId) throws UserException {
        synchronized (generalMutex) {
            logger.info("Try to get all users by customer id");
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(String.format(SELECT_USERS_BY_CUSTOMER, customerId.toString()));
                List<User> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(new User(new User.UserData(resultSet.getString("first_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("login"),
                            resultSet.getString("pass"),
                            User.UserData.UserRole.valueOf(resultSet.getString("user_role"))),
                            UUID.fromString(resultSet.getString("id")),
                            (UUID[])getUsersSubscriptions(UUID.fromString(resultSet.getString("id"))).toArray(), customerId));
                }
                return result;
            } catch (SQLException e) {
                logger.debug(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }
    public static List<Customer> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Try to get all customers");

            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_CUSTOMERS);
                List<Customer> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(new Customer(new Customer.CustomerData(
                                    resultSet.getString("first_name"),
                                    resultSet.getString("last_name"),
                                    resultSet.getString("login"),
                                    resultSet.getString("pass"),
                                    resultSet.getInt("money")), UUID.fromString(resultSet.getString("id")))
                            );
                }

                return result;
            } catch (SQLException e) {
                logger.debug(e.getMessage(), e);
                throw new RuntimeException(e);

            }
        }
    }

    public static UUID getIdByLogin(String login) {
        synchronized (generalMutex) {
            logger.info("Try to get id by login");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_CUSTOMER, login));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    Statement statement1 = connection.createStatement();
                    ResultSet rs1 = statement1.executeQuery(String.format(SELECT_USER, login));
                    if (rs1.next()) {
                        return UUID.fromString(rs1.getString(1));
                    } else {
                        throw new IllegalArgumentException("Customer or user with login '" + login + " was not found");
                    }
                }
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static Plan getPlanById(UUID id) throws PlanException {
        synchronized (generalMutex) {
            logger.info("Try to get plan by id");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_PLAN_BY_ID, id.toString()));
                if (rs.next()) {
                    return new Plan(UUID.fromString(rs.getString("id")), new Plan.PlanData(rs.getString("name"), rs.getString("details"),
                            rs.getInt("maxSeats"), rs.getInt("feePerUnit"), Plan.PlanType.valueOf(rs.getString("planType"))));
                } else {
                    throw new IllegalArgumentException("Plan with id '" + id + " was not found");
                }
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static Customer.CustomerData getCustomerDataById(UUID customerId) throws CustomerException {
        synchronized (generalMutex) {
            logger.info("Try to select customer data");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_DATA,
                                customerId));
                if (rs.next()) {
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    String login = rs.getString("login");
                    String pass = rs.getString("pass");
                    Integer money = rs.getInt("money");

                    return new Customer.CustomerData(firstName, lastName, login, pass, money);
                } else {
                    throw new IllegalArgumentException("Customer with id '" + customerId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static List<Subscription> getUsersSubscriptions(UUID userId) {
        synchronized (generalMutex) {
            logger.info("Try to get User's subscriptions");

            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(String.format(SELECT_SUBSCRIPTION_BY_USER_ID, userId.toString()));
                List<Subscription> res = new ArrayList<>();
                while (resultSet.next()) {
                    res.add(new Subscription(UUID.fromString(resultSet.getString("id")),
                            UUID.fromString(resultSet.getString("customer_id")),
                            UUID.fromString(resultSet.getString("plan_id")),
                            resultSet.getInt("used_seats"),
                            Subscription.SubscriptionType.valueOf(resultSet.getString("subscription_type"))));
                }
                return res;
            } catch (SQLException e) {
                logger.debug(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }
    public static List<Plan> getPlans() {
        synchronized (generalMutex) {
            logger.info("Try to get all plans");

            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_PLANS);
                List<Plan> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(new Plan(UUID.fromString(resultSet.getString("id")),
                            new Plan.PlanData(
                                    resultSet.getString("name"),
                                    resultSet.getString("details"),
                                    resultSet.getInt("max_seats"),
                                    resultSet.getInt("fee_per_seat"),
                                    Plan.PlanType.valueOf(resultSet.getString("plan_type")))));
                }

                return result;
            } catch (SQLException e) {
                logger.debug(e.getMessage(), e);
                throw new RuntimeException(e);

            }
        }
    }
    private static boolean comparePlans(UUID plan1, UUID plan2) {
        if (plan1.equals(plan2)) {
            throw new IllegalArgumentException("The plan with id \"" + plan1 + "\" was bought");
        }
        return true;
    }
    public static void buyPlan(UUID customerId, UUID planId) {
        synchronized (generalMutex) {
            logger.info("Try to buy plan");

            try {
                final Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(String.format(SELECT_PLAN_ID, customerId));

                while (resultSet.next()) {
                    if (resultSet.getString("plan_id").equals(planId.toString())) {
                        throw new IllegalArgumentException("The plan with id \"" + planId + "\" was bought");
                    }


                    if (comparePlans(UUID.fromString(resultSet.getString("plan_id")), planId)) {

                        resultSet = statement.executeQuery(String.format(SELECT_PLAN_TYPE, planId));

                        if (resultSet.getString("plan_type").equals("Internal")) {
                            statement.executeQuery(String.format(INSERT_SUBSCRIPTION, customerId, UUID.randomUUID(), planId, 0, "Done"));
                        } else {
                            final UUID newSubId = UUID.randomUUID();
                            statement.executeQuery(String.format(INSERT_SUBSCRIPTION, customerId, newSubId, planId, 0, "Provisioning"));
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(2000);
                                        statement.executeQuery(String.format(UPDATE_SUBSCRIPTION, "Done", newSubId));
                                    } catch (InterruptedException | SQLException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                        }

                    }

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
    public static List<Subscription> getSubscriptionsByCustomerId(UUID customerId) {
        synchronized (generalMutex) {
            logger.info("Try to get subscription by customer id");
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(String.format(SELECT_SUBSCRIPTION_BY_CUSTOMER_ID, customerId));
                List<Subscription> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(new Subscription(
                            UUID.fromString(resultSet.getString("id")),
                            UUID.fromString(resultSet.getString("customer_id")),
                            UUID.fromString(resultSet.getString("plan_id")),
                            resultSet.getInt("used_seats"),
                            Subscription.SubscriptionType.valueOf(resultSet.getString("subscription_type"))));
                }

                return result;
            } catch (SQLException e) {
                logger.debug(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }
    public static Customer getCustomerById(UUID customerId) {
        synchronized (generalMutex){
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_CUSTOMER_BY_ID, customerId));
                if(rs.next()) {
                    UUID id = UUID.fromString(rs.getString("id"));
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    String login = rs.getString("login");
                    Integer money = rs.getInt("money");
                    String pass = rs.getString("pass");

                    return new Customer(new Customer.CustomerData(firstName, lastName, login, pass, money), id);
                }
                else {
                    throw new IllegalArgumentException("Customer was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            } catch (CustomerException ex) {
                throw new RuntimeException("This should never happen, because we create customer from database data which was already verified");
            }
        }
    }

    public static void deleteUser(String login) {
        synchronized (generalMutex) {
            logger.info("Try to delete user");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                DELETE_USER,
                                login));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public static int updateBalance(UUID customerId, int amount) {
        synchronized (generalMutex) {
            logger.info("Try to update money on Customer by id");

            try {
                Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
                ResultSet rs = statement.executeQuery(
                        String.format(
                                "SELECT * FROM CUSTOMER WHERE id='%s'",
                                customerId.toString()));
                if(rs.next()) {
                    int total = rs.getInt("money") + amount;
                    if (total < 0)
                        throw new IllegalArgumentException("Insufficient funds!");

                    rs.updateInt("money", total);
                    rs.updateRow();
                    System.err.println("TOTAL " + total);
                    return total;
                }
                else {
                    throw new IllegalArgumentException("Customer with id '" + customerId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public static Subscription getSubscriptionById(UUID subscriptionId) {
        synchronized (generalMutex) {
            logger.info("Try to select subscription by id");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_PLAN_BY_ID, subscriptionId.toString()));
                if (rs.next()) {
                    return new Subscription(
                            UUID.fromString(rs.getString("id")),
                            UUID.fromString(rs.getString("customer_id")),
                            UUID.fromString(rs.getString("plan_id")),
                            rs.getInt("used_seats"),
                            Subscription.SubscriptionType.valueOf(rs.getString("subscription_type")));
                } else {
                    throw new IllegalArgumentException("Subscription with id '" + subscriptionId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public static User getUserById(UUID userId) {
        synchronized (generalMutex){
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_USER_BY_ID, userId));
                if(rs.next()) {
                    UUID id = UUID.fromString(rs.getString("id"));
                    UUID customerId = UUID.fromString(rs.getString("customer_id"));
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    String login = rs.getString("login");
                    String pass = rs.getString("pass");
                    User.UserData.UserRole userRole = User.UserData.UserRole.valueOf(rs.getString("user_role"));

                    rs = statement.executeQuery(String.format(SELECT_USER_COUNT_SUBSCRIPTIONS, id));
                    int count = 0;
                    if (rs.next())
                        count = rs.getInt(1);
                    UUID[] subscriptionIds = null;
                    if (count > 0) {
                        rs = statement.executeQuery(String.format(SELECT_USER_SUBSCRIPTIONS_BY_ID, id));
                        subscriptionIds = new UUID[count];
                        int i = 0;
                        while(rs.next()){
                            subscriptionIds[i] = (UUID.fromString(rs.getString("subscription_id")));
                            i++;
                        }
                    }
                    User user = new User(new User.UserData(firstName, lastName, login, pass, userRole), customerId, subscriptionIds, id);


                    return user;
                }
                else {
                    throw new IllegalArgumentException("User was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            } catch (UserException ex) {
                throw new RuntimeException("This should never happen, because we create customer from database data which was already verified");
            }
        }
    }
    public static void subscribeSubscriptionOnUser(UUID customerId, UUID userId, UUID subscriptionId) {
        Customer customer = DBService.getCustomerById(customerId);
        Subscription subscription = DBService.getSubscriptionById(subscriptionId);
        Plan plan = DBService.getPlanById(subscription.getPlanId());
        if(subscription.getSubscriptionType() == Subscription.SubscriptionType.PROVISIONING)
            throw new IllegalArgumentException("Can't assign provisioning subscriptions!");
        if(subscription.getUsedSeats() >= plan.getData().getMaxSeats())
            throw new IllegalArgumentException("Subscription is full!");
        if(customer.getData().getMoney() < plan.getData().getFeePerUnit())
            throw new IllegalArgumentException("Not enough money");
        DBService.updateBalance(customerId, -plan.getData().getFeePerUnit());

        synchronized(generalMutex){
            User user = getUserById(userId);
            if(ArrayUtils.contains(user.getSubscriptionIds(), subscriptionId))
                throw new IllegalArgumentException("user is already subscribed to this");
            else{

                try{
                    Statement statement = connection.createStatement();
                    statement.executeUpdate(
                            String.format(INSERT_USER_ASSIGNMENT,
                                    userId,
                                    subscriptionId));

                    changeSubscriptionSeats(subscriptionId, 1);
                }catch(SQLException ex){
                    logger.debug(ex.getMessage(), ex);
                    throw new RuntimeException(ex);
                }
            }
        }
    }
    public static void unsubscribeSubscriptionFromUser(UUID customerId, UUID userId, UUID subscriptionId) {
        synchronized(generalMutex){
            try{
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_ASSIGNMENT_SUBSCRIPTION,
                                userId, subscriptionId));
                if (rs.next()){
                    statement.executeUpdate(
                            String.format(
                                    DELETE_USER_ASSIGNMENT,
                                    userId,
                                    subscriptionId));

                    changeSubscriptionSeats(subscriptionId, -1);
                }
                else{
                    throw new IllegalArgumentException("The user doesn't have this subscription");
                }
            }catch(SQLException ex){
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public static void changeSubscriptionSeats(UUID subscriptionId, int amount){

        synchronized(generalMutex){
            Subscription subscription = getSubscriptionById(subscriptionId);
            try{
                Statement statement = connection.createStatement();

                ResultSet rs = statement.executeQuery(
                        String.format(
                                "SELECT * FROM SUBSCRIPTION WHERE id='%s'",
                                subscriptionId));
                if(rs.next()) {
                    int total = rs.getInt("used_seats") + amount;
                    if (total > getPlanById(subscription.getPlanId()).getData().getMaxSeats())
                        throw new IllegalArgumentException("Can't change subscription seats. Out of size");
                    rs.updateInt("used_seats", total);
                    rs.updateRow();
                    System.err.println("TOTAL " + total);
                }
            }catch(SQLException ex){
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public static User.UserData getUserDataById(UUID userId) {
        synchronized(generalMutex){
            try{
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_USER_BY_ID, userId));
                User.UserData userData = null;
                if(rs.next()) {
                    UUID id = UUID.fromString(rs.getString("id"));
                    UUID customerId = UUID.fromString(rs.getString("customer_id"));
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    String login = rs.getString("login");
                    String pass = rs.getString("pass");
                    User.UserData.UserRole userRole = User.UserData.UserRole.valueOf(rs.getString("user_role"));
                    userData = new User.UserData(firstName, lastName, login, pass, userRole);
                }
                return userData;
            }catch(SQLException ex){
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    private static void checkCustomerIdInUser(UUID customerId, UUID userId) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(
                String.format(
                        SELECT_ID_CUSTOMER_FROM_USER,
                        userId));

        if (!rs.next())
            throw new IllegalArgumentException("The user with id \"" + userId + "\" doesn't exist");
        else if (!rs.getString("id").equals(customerId.toString())) {
            throw new IllegalArgumentException("You can’t do something with foreign user");
        }
    }
    public static void changeRole(UUID customerId, UUID userId, User.UserData.UserRole role) {

        synchronized (generalMutex) {
            logger.info("Try to change Role");

            try {
                checkCustomerIdInUser(customerId, userId);
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_BY_ID,
                                userId));
                if(rs.next()) {
                    rs.updateString("role", role.toString());
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public static String getUserPasswordByLogin(String userLogin) {
        return getPasswordByLogin(userLogin, SELECT_USER_PASSWORD_BY_LOGIN);
    }
    private static String getPasswordByLogin(String login, String query) {
        synchronized (generalMutex) {
            logger.info("Try to find password by login");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                query,
                                login));
                if (rs.next())
                    return rs.getString("pass");
                else return null;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public static String getCustomerPasswordByLogin(String customerLogin) {
        return getPasswordByLogin(customerLogin, SELECT_CUSTOMER_PASSWORD_BY_LOGIN);
    }
    private static void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            logger.debug("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.debug("Failed to make connection!");
        }
    }
}
