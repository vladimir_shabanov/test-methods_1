package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.Validate;
import ru.nsu.fit.endpoint.service.database.Exceptions.CustomerException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Customer extends Entity<Customer.CustomerData>  {
    private UUID id;

    public Customer(CustomerData data, UUID id) {
        super(data);
        this.id = id;
        data.validate();
    }

    public UUID getId() {
        return id;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CustomerData {
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("firstName")
        private String firstName;

        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("lastName")
        private String lastName;

        /* указывается в виде email, проверить email на корректность */
        @JsonProperty("login")
        private String login;

        /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
        @JsonProperty("pass")
        private String pass;

        /* счет не может быть отрицательным */
        @JsonProperty("money")
        private int money;

        private CustomerData() {}

        public CustomerData(String firstName, String lastName, String login, String pass, int money) throws CustomerException {
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.pass = pass;
            this.money = money;

            validateName(firstName, "First");
            validateName(lastName, "Last");
            validateLogin(login);
            validatePassword(pass, login, firstName, lastName);
            validateMoney(money);
        }

        public void validate() {
            validate(firstName, lastName, login, pass, money);
        }

        public static void validate(String firstName, String lastName, String login, String pass, int money) {
            Validate.notNull(pass);
            Validate.isTrue(pass.length() >= 6 && pass.length() < 13, "Password's length should be more or equal 6 symbols and less or equal 12 symbols");
            Validate.isTrue(!pass.equalsIgnoreCase("123qwe"), "Password is easy");
            Validate.isTrue(money >= 0, "Money must be positive value");
        }

        public void validateName(String name, String type) throws CustomerException {
            if (name == null)
                throw new CustomerException(type + "name is null");
            if (name.length() > 12)
                throw new CustomerException(type + "name is too long");
            if (name.length() < 2)
                throw new CustomerException(type + "name is too short");
            if (name.contains(" "))
                throw new CustomerException(type + "name contains space char");
            if (name.charAt(0) < 'A' || name.charAt(0) > 'Z')
                throw new CustomerException(type + "name starts with incorrect symbol");
            for (int i = 1; i < name.length(); i++) {
                if (name.charAt(i) < 'a' || name.charAt(i) > 'z')
                    throw new CustomerException(type + "name contains incorrect symbols");
            }
        }

        public void validateLogin(String login) throws CustomerException {
            if (login == null)
                throw new CustomerException("Login is null");
            if (!login.contains("@"))
                throw new CustomerException("Invalid login");
            if (!login.contains("."))
                throw new CustomerException("Invalid login");
        }

        public void validatePassword(String password, String login, String firstName, String lastName) throws CustomerException {
            if (password == null)
                throw new CustomerException("Password is null");
            if (password.length() < 6)
                throw new CustomerException("Password is too short");
            if (password.length() > 12)
                throw new CustomerException("Password is too long");
            if (password.contains(firstName) || password.contains(lastName) || password.contains(login.substring(0, login.indexOf('@'))))
                throw new CustomerException("Password is insecure");
        }

        public void validateMoney(int money) throws CustomerException {
            if (money < 0)
                throw new CustomerException("Money has negative value");
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getLogin() {
            return login;
        }

        public String getPass() {
            return pass;
        }

        public int getMoney() {
            return money;
        }

        @Override
        public String toString() {
            return "CustomerData{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", login='" + login + '\'' +
                    ", pass='" + pass + '\'' +
                    ", money=" + money +
                    '}';
        }
    }
}
