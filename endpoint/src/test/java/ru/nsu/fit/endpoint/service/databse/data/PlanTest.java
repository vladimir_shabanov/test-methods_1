package ru.nsu.fit.endpoint.service.databse.data;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.Exceptions.PlanException;
import ru.nsu.fit.endpoint.service.database.data.Plan;

/**
 * Created by Vladimir on 24.10.2016.
 */
public class PlanTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewPlan() throws Exception {
        new Plan.PlanData("hello", "detail", 333, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanNullName() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Name is null");
        new Plan.PlanData(null, "detail", 333, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanShortName() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Name is too short");
        new Plan.PlanData("P", "detail", 333,  1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanShortName1() throws Exception {
        new Plan.PlanData("Pl", "detail", 333,  1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanLongName() throws Exception {
        new Plan.PlanData("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefgh", "detail", 333,  1000, Plan.PlanType.EXTERNAL);
    }

    public void testCreateNewPlanLongName1() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Name is too long");
        new Plan.PlanData("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghA", "detail", 333, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanWrongName() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Name contains special symbols");
        new Plan.PlanData("P111", "detail", 333, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanNullDetails() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Details are null");
        new Plan.PlanData("hello", null, 333, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanShortDetails() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Details are too short");
        new Plan.PlanData("hello", "", 333, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanShortDetails1() throws Exception {
        new Plan.PlanData("hello", "a", 333, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanSmallMaxSeats() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("MaxSeats value too small");
        new Plan.PlanData("hello", "a", 0, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanSmallMaxSeats1() throws Exception {
        new Plan.PlanData("hello", "a", 1,1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanBigMaxSeats() throws Exception {
        new Plan.PlanData("hello", "a", 999999, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanBigMaxSeats1() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("MaxSeats value too big");
        new Plan.PlanData("hello", "a", 1000000, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanSmallMinSeats1() throws Exception {
        new Plan.PlanData("hello", "a", 111, 1000, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanSmallFeePerUnit() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("feePerUnit too small");
        new Plan.PlanData("hello", "detail", 333,  -1, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanSmallFeePerUnit1() throws Exception {
        new Plan.PlanData("hello", "detail", 333,  0, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanBigFeePerUnit() throws Exception {
        new Plan.PlanData("hello", "detail", 333, 999999, Plan.PlanType.EXTERNAL);
    }

    @Test
    public void testCreateNewPlanBigFeePerUnit1() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("feePerUnit too big");
        new Plan.PlanData("hello", "detail", 333, 1000000, Plan.PlanType.EXTERNAL);
    }

}
