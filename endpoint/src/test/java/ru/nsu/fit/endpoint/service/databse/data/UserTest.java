package ru.nsu.fit.endpoint.service.databse.data;
import ru.nsu.fit.endpoint.service.database.data.User.UserData.UserRole;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.Exceptions.UserException;
import ru.nsu.fit.endpoint.service.database.data.User;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class UserTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() throws Exception {
        new User.UserData("John", "Wick", "john_wick@gmail.com", "strongpass", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithShortPass() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is too short");
        new User.UserData("John", "Wick", "john_wick@gmail.com", "12345", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithShortPass1() throws UserException {
        new User.UserData("John", "Wick", "john_wick@gmail.com", "123456", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithLongPass() throws UserException {
        new User.UserData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithLongPass1() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is too long");
        new User.UserData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithNullPass() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is null");
        new User.UserData("John", "Wick", "john_wick@gmail.com", null, User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass1() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is insecure");
        new User.UserData("John", "Wick", "john_wick@gmail.com", "Johnie", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass2() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is insecure");
        new User.UserData("John", "Wick", "john_wick@gmail.com", "Wickie", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass3() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is insecure");
        new User.UserData("John", "Wick", "john_wick@gmail.com", "john_wick", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithShortFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname is too short");
        new User.UserData("J", "Wick", "john_wick@gmail.com", "john_wick", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithShortFirstname1() throws UserException {
        new User.UserData("Jo", "Wick", "john_wick@gmail.com", "jon_wck", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstname1() throws UserException {
        new User.UserData("Johnjohnjohn", "Wick", "john_wick@gmail.com", "jon_wik", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname is too long");
        new User.UserData("Johnjohnjohnj", "Wick", "john_wick@gmail.com", "john_wick", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithSpaceInFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname contains space char");
        new User.UserData("Joh n", "Wick", "john_wick@gmail.com", "john_wick", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname starts with incorrect symbol");
        new User.UserData("john", "Wick", "john_wick@gmail.com", "john_wick", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname contains incorrect symbols");
        new User.UserData("JOHN", "Wick", "john_wick@gmail.com", "john_wick", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithShortLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname is too short");
        new User.UserData("John", "W", "john_wick@gmail.com", "john_wick", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithShortLastname1() throws UserException {
        new User.UserData("John", "Wi", "john_wick@gmail.com", "joen_wiwk", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithLongLastname1() throws UserException {
        new User.UserData("John", "Wickwickwick", "john_wick@gmail.com", "jodhn_weick", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithLongLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname is too long");
        new User.UserData("John", "Wickwickwick1", "john_wick@gmail.com", "johg_wieck", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithSpaceInLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname contains space char");
        new User.UserData("John", "Wic k", "john_wick@gmail.com", "joghn_wigck", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname starts with incorrect symbol");
        new User.UserData("John", "wick", "john_wick@gmail.com", "johgn_wigck", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname contains incorrect symbols");
        new User.UserData("John", "WICK", "john_wick@gmail.com", "johgn_wicgk", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Invalid login");
        new User.UserData("John", "Wick", "john_wickgmail.com", "jon_wck", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin1() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Invalid login");
        new User.UserData("John", "Wick", "john_wick@gmailcom", "jon_wck", User.UserData.UserRole.USER);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin2() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Login is null");
        new User.UserData("John", "Wick", null, "jon_wck", User.UserData.UserRole.USER);
    }
}
