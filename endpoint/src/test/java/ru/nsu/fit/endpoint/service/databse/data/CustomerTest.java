package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.Exceptions.CustomerException;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() throws Exception {
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "strongpass", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Password is too short");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "12345", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass1() throws CustomerException {
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123456", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongPass() throws CustomerException {
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongPass1() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Password is too long");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1", 0);
    }

    @Test
    public void testCreateNewCustomerWithNullPass() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Password is null");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", null, 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass1() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Password is insecure");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "Johnie", 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass2() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Password is insecure");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "Wickie", 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass3() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Password is insecure");
        new Customer.CustomerData("John", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortFirstname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Firstname is too short");
        new Customer.CustomerData("J", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortFirstname1() throws CustomerException {
        new Customer.CustomerData("Jo", "Wick", "john_wick@gmail.com", "jon_wck", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstname1() throws CustomerException {
        new Customer.CustomerData("Johnjohnjohn", "Wick", "john_wick@gmail.com", "jon_wik", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Firstname is too long");
        new Customer.CustomerData("Johnjohnjohnj", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithSpaceInFirstname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Firstname contains space char");
        new Customer.CustomerData("Joh n", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInFirstname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Firstname starts with incorrect symbol");
        new Customer.CustomerData("john", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInFirstname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Firstname contains incorrect symbols");
        new Customer.CustomerData("JOHN", "Wick", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortLastname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Lastname is too short");
        new Customer.CustomerData("John", "W", "john_wick@gmail.com", "john_wick", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortLastname1() throws CustomerException {
        new Customer.CustomerData("John", "Wi", "john_wick@gmail.com", "joen_wiwk", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongLastname1() throws CustomerException {
        new Customer.CustomerData("John", "Wickwickwick", "john_wick@gmail.com", "jodhn_weick", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongLastname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Lastname is too long");
        new Customer.CustomerData("John", "Wickwickwick1", "john_wick@gmail.com", "johg_wieck", 0);
    }

    @Test
    public void testCreateNewCustomerWithSpaceInLastname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Lastname contains space char");
        new Customer.CustomerData("John", "Wic k", "john_wick@gmail.com", "joghn_wigck", 0);
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInLastname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Lastname starts with incorrect symbol");
        new Customer.CustomerData("John", "wick", "john_wick@gmail.com", "johgn_wigck", 0);
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInLastname() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Lastname contains incorrect symbols");
        new Customer.CustomerData("John", "WICK", "john_wick@gmail.com", "johgn_wicgk", 0);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Invalid login");
        new Customer.CustomerData("John", "Wick", "john_wickgmail.com", "jon_wck", 0);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin1() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Invalid login");
        new Customer.CustomerData("John", "Wick", "john_wick@gmailcom", "jon_wck", 0);
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin2() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Login is null");
        new Customer.CustomerData("John", "Wick", null, "jon_wck", 0);
    }

    @Test
    public void testCreateNewCustomerNegativeMoney() throws CustomerException {
        expectedEx.expect(CustomerException.class);
        expectedEx.expectMessage("Money has negative value");
        new Customer.CustomerData("John", "Wick", "suka@blyat.nah", "jon_wck", -1);
    }

    @Test
    public void testCreateNewCustomerNegativeMoney1() throws CustomerException {
        new Customer.CustomerData("John", "Wick", "suka@blyat.nah", "jon_wck", 0);
    }


}
